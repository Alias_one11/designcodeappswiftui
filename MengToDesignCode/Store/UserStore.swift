import SwiftUI
import Combine

class UserStore: ObservableObject {
	// MARK: - ©Global-PROPERTIES
	/*****************************/
	@Published var isLoggedIn: Bool = UserDefaults
	.standard.bool(forKey: "isLoggedIn") {
		// - Persisting data in the app to remain
		// - logged in when we close the app
		didSet {
			UserDefaults.standard.set(isLoggedIn, forKey: "isLoggedIn")
		}
	}
	
	@Published var showLogin: Bool = false

	/*****************************/
	
	
}///END-Class
