import SwiftUI
import Combine

class DataStore: ObservableObject {
	// MARK: - ©Global-PROPERTIES
	/*****************************/
	// MARK: - ©Source of truth 🏦  🔌
	@Published var posts: [Post] = []
	/*****************************/
	
	// MARK: - ©init--> Will run this function when it is initialized
	/**©-------------------------------------------©*/
	init() { storeGetPost() }
	/**©-------------------------------------------©*/
	
	// MARK: - Class methods
	func storeGetPost() {
		API().getPosts { posts in
			self.posts = posts
		}
	}
	
}
