import SwiftUI
import Combine

class UpdateStore: ObservableObject {
	// MARK: - ©PROPERTIES
	// - Allows our data to be constantly
	// - updated throughout the app
	@Published var updates: [Update] = updateData
}
