import SwiftUI
import Contentful
import Combine

let spaceId = "pbrco31954a9"
let accessToken = "UNdKnYKEBRm8MOER4ywy-U5b3ZQGuUjakMjOb198BLE"

let client = Client(spaceId: spaceId, accessToken: accessToken)

func getArray(id: String, completion: @escaping ([Entry]) -> Void) {
	///__________
	let query = Query.where(contentTypeId: id)
	
	client.fetchArray(of: Entry.self, matching: query) { result in
		///__________
		DispatchQueue.main.async {
			switch result {
				
				case .success(let array):
					completion(array.items)
				case .failure(let error):
					printf(error)
			}
		}
	}
}

/*©-----------------------------------------©*/
class CourseStore: ObservableObject {
	// MARK: - ©Global-PROPERTIES
	/*****************************/
	@Published var courses: [Course] = courseData
	/*****************************/
	
	// MARK: - ©init
	/**©-------------------------------------------©*/
	init() {
		
		let colors = [ #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1), #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1), #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1), #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1), #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1), #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1), #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1) ]
		var colorIndex: Int = 0
		
		getArray(id: "course11") { items in
			///__________
			items.forEach { item in
				///__________
				self.courses.append(
					Course(title: item.fields["title"] as! String,
						   subtitle: item.fields["subtitle"] as! String,
						   image: item.fields
							.linkedAsset(at: "image")?.url ?? URL(string: "")!,
						   logo: #imageLiteral(resourceName: "Logo1"),
						   color: colors[colorIndex],
						   show: false)
				)
				
				colorIndex += 1
			}///END-ForEach
		}
	}
	/**©-------------------------------------------©*/
}

