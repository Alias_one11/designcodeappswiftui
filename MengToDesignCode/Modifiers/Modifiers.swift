import SwiftUI

// MARK: - MODIFIERS
/*****************************/


struct ShadowModifier: ViewModifier {
	// MARK: - ©Body
	/*****************************/
	func body(content: Content) -> some View {
		///__________
		content
			.shadow(color: Color.black.opacity(0.1),
					   radius: 10, x: 0, y: 12)
			.shadow(color: Color.black.opacity(0.1),
					radius: 1, x: 0, y: 1)
	}
	/*****************************/
	
}// END: [STRUCT]
///*©-----------------------------------------©*/

struct FontModifier: ViewModifier {
	// MARK: - ©PROPERTIES
	/*****************************/
	var style: Font.TextStyle = .body
	var design: Font.Design = .default
	
	// MARK: - ©body() -> some View
	/*****************************/
	func body(content: Content) -> some View {
		///__________
		content.font(.system(style, design: design))
	}
	/*****************************/
}// END: [STRUCT]
///*©-----------------------------------------©*/

struct CustomFontModifier: ViewModifier {
	// MARK: - ©PROPERTIES
	/*****************************/
	var customFont: String
	var size: CGFloat = 28
	
	// MARK: - ©body() -> some View
	/*****************************/
	func body(content: Content) -> some View {
		content.font(.custom(customFont, size: size))
	}
	/*****************************/
}
