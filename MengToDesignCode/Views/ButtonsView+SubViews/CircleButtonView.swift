import SwiftUI

// MARK: - Preview
struct CircleButtonView_Previews: PreviewProvider {
	
	static var previews: some View {
		
		CircleButtonView()//.padding(.all, 100)
			//.preferredColorScheme(.dark)
			//.previewLayout(.sizeThatFits)
			.previewLayout(.fixed(width: 380, height: 680))
	}
}

struct CircleButtonView: View {
	// MARK: - ©Global-PROPERTIES
	/*****************************/
	// MARK: - ©State-->PROPERTIES
	@State private var tap: Bool = false
	@State private var press: Bool = false
	/*****************************/
	
	/*©-----------------------------------------©*/
	var body: some View {
		
		//||_PARENT__=>ZSTACK||
		//*****************************/
		ZStack {
			// - Starting top icon
			// MARK: - SFSymbol sun.max
			Image(systemName: "sun.max")
				.font(.system(size: 44, weight: .light))
				//__________
				// MARK: - 3d rotating animation effect
				///@`````````````````````````````````````````````
				// - Animation offset of the sf symbol when the button is pressed
				// - So the image will slide off the button when long pressed
				.offset(x: press ? -90 : 0, y: press ? -90 : 0)
				.rotation3DEffect(Angle(degrees: press ? 20 : 0),
								  axis: (x: 10.0, y: -10.0, z: 0.0))
				///@-`````````````````````````````````````````````
				//_________
			
			// - Layering the moon on top of the view but on the other side
			// - When the animation rotates, the moon will replace the sun icon
			// MARK: - SFSymbol moon
			Image(systemName: "moon")
				.font(.system(size: 44, weight: .light))
				//__________
				// MARK: - 3d rotating animation effect
				///@`````````````````````````````````````````````
				// - Animation offset of the sf symbol when the button is pressed
				// - So the image will slide off the button when long pressed
				.offset(x: press ? 0 : 90, y: press ? 0 : 90)
				.rotation3DEffect(Angle(degrees: press ? 0 : 20),
								  axis: (x: -10.0, y: 10.0, z: 0.0))
			///@-`````````````````````````````````````````````
			//_________
		}//||END__PARENT-ZSTACK||
		.frame(width: 100, height: 100)
		//__________
		// MARK: - .background
		///@`````````````````````````````````````````````
		.background(
			ZStack {
				LinearGradient(gradient:
								Gradient(colors: [
									Color(press ?  #colorLiteral(red: 0.8980392157, green: 0.9333333333, blue: 1, alpha: 1) : #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)),
									Color(press ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) : #colorLiteral(red: 0.8980392157, green: 0.9333333333, blue: 1, alpha: 1))
								]),
							   startPoint: .topLeading, endPoint: .bottomTrailing
				)
				
				Circle()
					.stroke(Color.clear, lineWidth: 10)
					.shadow(color:Color(press ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) : #colorLiteral(red: 0.8980392157, green: 0.9333333333, blue: 1, alpha: 1)), radius: 3, x: -5, y: -5)
				
				Circle()
					.stroke(Color.clear, lineWidth: 10)
					.shadow(color: Color(press ?  #colorLiteral(red: 0.8980392157, green: 0.9333333333, blue: 1, alpha: 1) : #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)), radius: 3, x: 3, y: 3)
			}
		)///END-.background
		///@`````````````````````````````````````````````
		//_________
		.clipShape(Circle())
		.shadow(color: Color(press ?  #colorLiteral(red: 0.8980392157, green: 0.9333333333, blue: 1, alpha: 1) : #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)),radius: 20, x: -20, y: -20)
		.shadow(color: Color(press ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) : #colorLiteral(red: 0.8980392157, green: 0.9333333333, blue: 1, alpha: 1)),radius: 20, x: 20, y: 20)
		//__________
		.scaleEffect(tap ? 1.2 : 1)
		// MARK: - LongPressGesture
		///@`````````````````````````````````````````````
		.gesture(
			LongPressGesture(minimumDuration: 0.1, maximumDistance: 10)
				.onChanged({ _ in
				///__________
				tap = true
				// - Resets the buttons state
				DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
					tap = false
				}
			})
			// - For the long press
			.onEnded({ value in
				///__________
				press.toggle()
			})
			///__________
		)///END-.gesture
		///@-`````````````````````````````````````````````
		//_________
		//*****************************/
		
	}///-||End Of body||
	/*©-----------------------------------------©*/
}// END: [STRUCT]
