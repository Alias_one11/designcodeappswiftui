import SwiftUI

// MARK: - Haptic-feedback
///@`````````````````````````````````````````````
func haptic(type: UINotificationFeedbackGenerator.FeedbackType) {
	///__________
	UINotificationFeedbackGenerator().notificationOccurred(type)
}

func impact(type: UIImpactFeedbackGenerator.FeedbackStyle) {
	///__________
	UIImpactFeedbackGenerator(style: type).impactOccurred()
}
///@-`````````````````````````````````````````````

// MARK: - Preview
struct ButtonsView_Previews: PreviewProvider {
	
	static var previews: some View {
		
		ButtonsView()//.padding(.all, 100)
			//.preferredColorScheme(.dark)
			//.previewLayout(.sizeThatFits)
			.previewLayout(.fixed(width: 380, height: 680))
	}
}

struct ButtonsView: View {
	// MARK: - ©Global-PROPERTIES
	/*****************************/
	

	/*****************************/
	
	/*©-----------------------------------------©*/
	var body: some View {
		
		//||_PARENT__=>VSTACK||
		//*****************************/
		VStack(spacing: 50) {
			
			///_CHILD__=>RectangleButtonView()
			/*****************************/
			RectangleButtonView()
			/*****************************/
			
			///_CHILD__=>CircleButtonView()
			/*****************************/
			CircleButtonView()
			/*****************************/
			
			///_CHILD__=>PayButtonView()
			/*****************************/
			PayButtonView()
			/*****************************/
			
			
		}//||END__PARENT-VSTACK||
		.frame(maxWidth: .infinity, maxHeight: .infinity)
		.background(Color(#colorLiteral(red: 0.8980392157, green: 0.9333333333, blue: 1, alpha: 1)))
		.edgesIgnoringSafeArea(.all)
		//__________
		// MARK: - animation & spring effect animations
		///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		.animation(
			.spring(response: 0.5, dampingFraction: 0.5, blendDuration: 0)
		)
		///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		//_________
		//*****************************/
		
	}///-||End Of body||
	/*©-----------------------------------------©*/
}// END: [STRUCT]
