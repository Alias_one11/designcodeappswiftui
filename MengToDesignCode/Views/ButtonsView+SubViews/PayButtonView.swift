import SwiftUI

// MARK: - Preview
struct PayButtonView_Previews: PreviewProvider {
	
	static var previews: some View {
		
		PayButtonView()//.padding(.all, 100)
			//.preferredColorScheme(.dark)
			//.previewLayout(.sizeThatFits)
			.previewLayout(.fixed(width: 380, height: 680))
	}
}

struct PayButtonView: View {
	// MARK: - ©Global-PROPERTIES
	/*****************************/
	// MARK: - ©State-->PROPERTIES
	@GestureState private var tap: Bool = false
	@State private var press: Bool = false
	/*****************************/
	
	/*©-----------------------------------------©*/
	var body: some View {
		
		//||_PARENT__=>ZSTACK||
		//*****************************/
		ZStack {
			
			// MARK: - Fingerprint image #1
			Image("fingerprint")
				.opacity(press ? 0 : 1)
				.scaleEffect(press ? 0 : 1)
			
			// MARK: - Fingerprint image #2
			Image("fingerprint2")
				// - Will drop this image 50 points up in the,
				// - y axis when the button is not tapped, then
				// - set it to 0 on top of the other image when tapped
				.clipShape(Rectangle().offset(y: tap ? 0 : 50))
				.animation(.easeInOut)
				.opacity(press ? 0 : 1)
				.scaleEffect(press ? 0 : 1)

			// MARK: - SFSymbol check mark
			Image(systemName: "checkmark.circle.fill")
				.font(.system(size: 44, weight: .light))
				.foregroundColor(Color(#colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1)))
				.opacity(press ? 1 : 0)
				// - Scales the check mark from big bag to small
				.scaleEffect(press ? 1 : 0)

			//_________
		}//||END__PARENT-ZSTACK||
		.frame(width: 120, height: 120)
		//__________
		// MARK: - .background
		///@`````````````````````````````````````````````
		.background(
			ZStack {
				LinearGradient(gradient:
								Gradient(colors: [
									Color(press ?  #colorLiteral(red: 0.8980392157, green: 0.9333333333, blue: 1, alpha: 1) : #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)),
									Color(press ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) : #colorLiteral(red: 0.8980392157, green: 0.9333333333, blue: 1, alpha: 1))
								]),
							   startPoint: .topLeading, endPoint: .bottomTrailing
				)
				
				Circle()
					.stroke(Color.clear, lineWidth: 10)
					.shadow(color:Color(press ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) : #colorLiteral(red: 0.8980392157, green: 0.9333333333, blue: 1, alpha: 1)), radius: 3, x: -5, y: -5)
				
				Circle()
					.stroke(Color.clear, lineWidth: 10)
					.shadow(color: Color(press ?  #colorLiteral(red: 0.8980392157, green: 0.9333333333, blue: 1, alpha: 1) : #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)), radius: 3, x: 3, y: 3)
			}
		)///END-.background
		///@`````````````````````````````````````````````
		//_________
		.clipShape(Circle())
		.overlay(
			Circle()
				.trim(from: tap ? 0.001 : 1, to: 1)
				.stroke(
					LinearGradient(gradient:
									Gradient(colors: [Color(#colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1)), Color(#colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1))]),
								   startPoint: .topLeading, endPoint: .bottomTrailing),
						style: StrokeStyle(lineWidth: 5,
										   lineCap: .round)
				)
				.frame(width: 88, height: 88)
				.rotationEffect(Angle(degrees: 90))
				.rotation3DEffect(Angle(degrees: 180), axis: (x: 1.0, y: 0.0, z: 0.0))
				.shadow(color: Color(#colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1)).opacity(0.3), radius: 5, x: 3, y: 3)
				.animation(.easeInOut)
		)///END-.overlay
		///__________
		.shadow(color: Color(press ?  #colorLiteral(red: 0.8980392157, green: 0.9333333333, blue: 1, alpha: 1) : #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)),radius: 20, x: -20, y: -20)
		.shadow(color: Color(press ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) : #colorLiteral(red: 0.8980392157, green: 0.9333333333, blue: 1, alpha: 1)),radius: 20, x: 20, y: 20)
		//__________
		.scaleEffect(tap ? 1.2 : 1)
		// MARK: - LongPressGesture
		///@`````````````````````````````````````````````
		.gesture(
			LongPressGesture()
				// - The difference when using @Gesture state is the longer
				// - you tap, the bigger the button expands
				.updating($tap, body: { (currentState, gestureState, transaction) in
					///__________
					gestureState = currentState
				})
				
				// - For the long press
				.onEnded({ value in
					///__________
					press.toggle()
				})
			///__________
		)///END-.gesture
		///@-`````````````````````````````````````````````
		//_________
		//*****************************/
		
	}///-||End Of body||
	/*©-----------------------------------------©*/
}// END: [STRUCT]

