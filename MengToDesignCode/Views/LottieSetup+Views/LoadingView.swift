import SwiftUI

// MARK: - Preview
struct LoadingView_Previews: PreviewProvider {
	
	static var previews: some View {
		
		LoadingView()//.padding(.all, 100)
			//.preferredColorScheme(.dark)
			//.previewLayout(.sizeThatFits)
			.previewLayout(.fixed(width: 375, height: 780))
	}
}

struct LoadingView: View {
	// MARK: - ©Global-PROPERTIES
	/*****************************/
	
	/*****************************/
	
	/*©-----------------------------------------©*/
	var body: some View {
		
		//||_PARENT__=>VSTACK||
		//*****************************/
		VStack {
			
			LottieView(filename: "loading3")
				.frame(width: 200.0, height: 200.0)
			
		}//||END__PARENT-VSTACK||
		//*****************************/
		
	}///-||End Of body||
	/*©-----------------------------------------©*/
}// END: [STRUCT]
