import SwiftUI
import Lottie

struct LottieView: UIViewRepresentable {
	// MARK: - typealias
	typealias UIViewType = UIView
	
	// MARK: - ©Global-PROPERTIES
	/*****************************/
	var filename: String
	
	var animationView = AnimationView()
	/*****************************/
	
	func makeUIView(context: UIViewRepresentableContext<LottieView>) -> UIView {
		// - .zero or an x, y,z axis at (0,0) or the middle
		let view = UIView(frame: .zero)
		
		// MARK: - Lottie code
		// - Animation file name
		animationView.animation = Animation.named(filename)
		
		// - Making sure our animation scales properly
		animationView.contentMode = .scaleAspectFit
//		animationView.loopMode = .loop
		animationView.play()
		
		view.addSubview(animationView)

		// - Dealing with constraints
		animationView.translatesAutoresizingMaskIntoConstraints = false
//
		NSLayoutConstraint.activate([
			// - Width
			animationView.widthAnchor.constraint(equalTo: view.widthAnchor),
			// - Height
			animationView.heightAnchor.constraint(equalTo: view.heightAnchor)
		])
		
		return view
	}
	
	func updateUIView(_ uiView: UIView,
					  context: UIViewRepresentableContext<LottieView>) {
		// - We are inputting no code here
	}
}// END: [STRUCT]
