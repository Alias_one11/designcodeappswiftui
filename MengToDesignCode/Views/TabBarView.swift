import SwiftUI

// MARK: - Preview
struct TabBarView_Previews: PreviewProvider {
	
	static var previews: some View {
		
		TabBarView()//.padding(.all, 100)
			//.preferredColorScheme(.dark)
			//.previewLayout(.sizeThatFits)
			.environmentObject(UserStore())
			.previewLayout(.fixed(width: 375, height: 780))
	}
}

struct TabBarView: View {
	/// MARK: - ©Global-PROPERTIES
	/*****************************/
	
	/*****************************/
	
	/*©-----------------------------------------©*/
	var body: some View {
		
		//||_PARENT__=>VSTACK||
		//*****************************/
			TabView {
			///__________
			// - Home tab
			MainView().tabItem {
				Image(systemName: "play.circle.fill")
				Text("Home")
			}
			// - Certificates tab
			CourseListView().tabItem {
				Image(systemName: "rectangle.stack.fill")
				Text("Courses")
			}
			
			
			
		}//||END__PARENT-VSTACK||
		
		//*****************************/
		
	}///-||End Of body||
	/*©-----------------------------------------©*/
}// END: [STRUCT]
