import SwiftUI

struct UpdateListView: View {
	// MARK: - ©ObservedObject-PROPERTIES
	@ObservedObject var store = UpdateStore()
	
	/**©---------------------------------------------------------------©*/
	// MARK: - Helper methods
	func addUpdate() {
		///|__________|
		store.updates.append(
			Update(image: "Card1", title: "New Item",
				   text: "text", date: "Jan 1"))
	}
	
	/*©-----------------------------------------©*/
	var body: some View {
		
		NavigationView {
			///__________
			List {
				ForEach(store.updates) { update in
					///__________
					NavigationLink(destination: UpdateDetailView(update: update)) {
						
						HStack {
							// - Image<-- horizontally shown
							Image(update.image).resizable()
								.aspectRatio(contentMode: .fit)
								.frame(width: 80.0, height: 80.0)
								.background(Color.black)
								.cornerRadius(20)
								.padding(.trailing, 4)
							///__________
							VStack(alignment: .leading, spacing: 8.0) {
								// - TITLE
								Text(update.title)
									.font(.system(size: 20, weight: .bold))
								
								// - TEXT
								Text(update.text).lineLimit(2)
									.font(.subheadline)
									.foregroundColor(Color(#colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)))
								
								// - DATE
								Text(update.date)
									.font(.caption)
									.fontWeight(.bold)
									.foregroundColor(.secondary)
								
							}///_END__=>VStack
							///__________
						}///_END__=>HStack
						.padding(.vertical, 8)
						///*------------------------------*/
						
					}///_END__=>NavigationLink
					///__________
				}///_END__=>ForEach
				///__________
				// - Deletes items in the list
				///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
				.onDelete { index in
					store.updates.remove(at: index.first!)
				}
				/// - |.onMove| ⥥
				/// - The closure takes two arguments that represent the offset
				/// - relative to the dynamic view’s underlying collection of data.
				/// - Pass nil to disable the ability to move items.
				/// - Basically you can drag a cell when edit is pressed
				/// - to delete an item in the list
				.onMove { (source: IndexSet, destination: Int) in
					///__________
					store.updates.move(fromOffsets: source, toOffset: destination)
				}
				///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
				///__________
			}//_END__=>PARENT<>List
			///__________
			.navigationBarTitle("Updates")
			///__________
			// - Will add a new bar button that adds new updates when pressed
			///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
			///__________
			.navigationBarItems(
				/// - Add Update button
				leading: Button(action: addUpdate,
								label: {
									Text("Add Update")
								}
				), trailing: EditButton()/// - Edit to delete button
			)//_END.navigationBarItems
			///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
			
			///*------------------------------*/
			
			
		}///_END__=>NAVIGATIONVIEW
		.navigationViewStyle(StackNavigationViewStyle())
		///*------------------------------*/
		
	}
	/*©-----------------------------------------©*/
}// END: [STRUCT]

struct UpdateListView_Previews: PreviewProvider {
	
	static var previews: some View {
		
		UpdateListView()//.padding(.all, 100)
		//.preferredColorScheme(.dark)
		//.previewLayout(.sizeThatFits)
		//.previewLayout(.fixed(width: 540, height: 640))
	}
}
