import SwiftUI
import Firebase

// MARK: - Preview
struct LoginView_Previews: PreviewProvider {
	
	static var previews: some View {
		
		LoginView()//.padding(.all, 100)
			//.preferredColorScheme(.dark)
			//.previewLayout(.sizeThatFits)
			.previewLayout(.fixed(width: 375, height: 780))
	}
}

struct LoginView: View {
	// MARK: - ©Global-PROPERTIES
	/*****************************/
	// MARK: - ©State-->PROPERTIES
	@State private var email: String = ""
	@State private var password: String = ""
	@State private var isFocused: Bool = false
	@State private var showAlert: Bool = false
	@State private var alertMsg: String = "Incorrect format..."
	@State private var isLoading: Bool = false
	@State private var isSuccessful: Bool = false
	
	// MARK: - ©EnvironmentObject-->PROPERTIES
	@EnvironmentObject private var user: UserStore
	
	/*****************************/
	
	// MARK: - Helper methods
	/**©------------------------------------------------------------------©*/
	func hideKeyBoard() {
		UIApplication.shared.sendAction(
			#selector(UIResponder.resignFirstResponder),
			to: nil, from: nil, for: nil)
	}
	
	// MARK: - Login()
	///*------------------------------*/
	func login() {
		///__________
		hideKeyBoard()
		isFocused = false
		///__________
		// MARK: - Firebase
		///@`````````````````````````````````````````````
		Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
			// - Handling error
			if let error = error {
				///__________
				isLoading = false
				
				alertMsg = error.localizedDescription
				printf("Cannot sign in..\n\(alertMsg)")
				
				return showAlert = true
				
			} else {
				// - Was successful in signing in
				isSuccessful = true
				// - Shows that the login was successful
				user.isLoggedIn = true
				// - Setting up data persistence to save the log in state
				// - and keep us logged in once the app is closed
				UserDefaults.standard.set(true, forKey: "isLoggedIn")
				
				printf("Login was successful...")
				
				///__________
				DispatchQueue.main.asyncAfter(deadline: .now() + 4.2) {
					///__________
					
					isSuccessful = false
					
					// - Reset the input fields
					email = ""
					password = ""
					
					///__________
				}///END--->DispatchQueue
				
				DispatchQueue.main.asyncAfter(deadline: .now() + 4.2) {
					/// Lottie loading animation
					isLoading = true
					
					DispatchQueue.main.asyncAfter(deadline: .now() + 3.2) {
						///__________
						/// - Pops off the view login screen once login is successful
						user.showLogin = false
					}///END-Child-->DispatchQueue
				}//END-Parent-->DispatchQueue
			}
		}///END-Auth
		///@-`````````````````````````````````````````````
		//_________
	}
	///*------------------------------*/
	/*©-----------------------------------------©*/
	
	
	var body: some View {
		
		//||_PARENT__=>ZSTACK||
		//*****************************/
		ZStack {
			Color.black.edgesIgnoringSafeArea(.all)
			
			///_CHILD__=>ZStack
			/*****************************/
			ZStack(alignment: .top) {
				
				///_CHILD__=>Color("background2")
				/*****************************/
				
				Color("background2")
					// MARK: - clipShape corner radius
					///@`````````````````````````````````````````````
					.clipShape(
						RoundedRectangle(cornerRadius: 30, style: .continuous)
					)
					///@-`````````````````````````````````````````````
					.edgesIgnoringSafeArea(.bottom)
				/*****************************/
				
				///_CHILD__=>CoverView()
				/*****************************/
				CoverView()
				/*****************************/
				
				///_CHILD__=>VStack
				/*****************************/
				VStack {
					///_CHILD__=>HStack
					/*****************************/
					HStack {
						// MARK: - Person SFSymbol
						Image(systemName: "person.crop.circle.fill")
							.foregroundColor(Color(#colorLiteral(red: 0.6549019608, green: 0.7137254902, blue: 0.862745098, alpha: 1)))
							.frame(width: 44, height: 44)
							.background(Color.white)
							.clipShape(
								RoundedRectangle(cornerRadius: 16,
												 style: .continuous)
							)
							.shadow(color: Color.black.opacity(0.15), radius: 5, x: 0, y: 5)
							.padding(.leading)
						
						// MARK: - Email TextField
						TextField("Your Email".uppercased(), text: $email)
							.keyboardType(.emailAddress)
							.font(.subheadline)
							.padding(.leading)
							.frame(height: 44)
							//__________
							// MARK: - onTapGesture
							///@`````````````````````````````````````````````
							.onTapGesture {
								isFocused = true
							}
						///@-`````````````````````````````````````````````
						//_________
						
					}///END-HStack
					/*****************************/
					
					// - Places a dividing line in between the two field inputs
					Divider().padding(.leading, 80)
					
					///_CHILD__=>HStack
					/*****************************/
					HStack {
						// MARK: - Lock SFSymbol
						Image(systemName: "lock.fill")
							.foregroundColor(Color(#colorLiteral(red: 0.6549019608, green: 0.7137254902, blue: 0.862745098, alpha: 1)))
							.frame(width: 44, height: 44)
							.background(Color.white)
							.clipShape(
								RoundedRectangle(cornerRadius: 16,
												 style: .continuous)
							)
							.shadow(color: Color.black.opacity(0.15), radius: 5, x: 0, y: 5)
							.padding(.leading)
						
						// MARK: - Password SecureField
						SecureField("Password".uppercased(), text: $password)
							.keyboardType(.default)
							.font(.subheadline)
							.padding(.leading)
							.frame(height: 44)
							// MARK: - onTapGesture
							///@`````````````````````````````````````````````
							.onTapGesture(perform: {
								isFocused = true
							})
						///@-`````````````````````````````````````````````
						//_________
						
					}///END-HStack
					/*****************************/
				}///END-VStack
				.frame(height: 136)
				.frame(maxWidth: 712)
				.background(BlurView(style: .systemMaterial))
				.clipShape(
					RoundedRectangle(cornerRadius: 30, style: .continuous)
				)
				.shadow(color: Color.black.opacity(0.15), radius: 20, x: 0, y: 20)
				.padding(.horizontal)
				// - Moves the frame down
				.offset(y: 470)
				/*****************************/
				
				///_CHILD__=>HStack
				/*****************************/
				HStack {
					// MARK: - Forgot password Text
					Text("Forgot password?").bold()
						.font(.subheadline)
					
					
					// - Spaced horizontally
					Spacer()
					
					// MARK: - Button
					Button(action: { login() }
						   , label: {
							Text("Log in")
								.foregroundColor(.black)
						}
					)///END-Button
					///__________
					.padding(12)
					.padding(.horizontal, 30)
					.background(Color(#colorLiteral(red: 0, green: 0.7529411765, blue: 1, alpha: 1)))
					.clipShape(RoundedRectangle(cornerRadius: 20, style: .continuous))
					.shadow(color: Color(#colorLiteral(red: 0, green: 0.7529411765, blue: 1, alpha: 1)).opacity(0.3), radius: 20, x: 0, y: 20)
					// MARK: - alert
					///@`````````````````````````````````````````````
					.alert(isPresented: $showAlert, content: {
						///__________
						Alert(title: Text("Error"),
							  message: Text(alertMsg),
							  dismissButton: .default(Text("OK")))
					})
					///@-`````````````````````````````````````````````
					//_________
					/*****************************/
					
				}///END-HStack
				.frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .bottom)
				.padding()
				/*****************************/
				
				
			}///END-ZStack
			.offset(y: isFocused ? -270 : 0)
			.animation(isFocused ? .easeInOut : nil)
			// MARK: - onTapGesture
			///@`````````````````````````````````````````````
			.onTapGesture {
				isFocused = false
				hideKeyBoard()
			}
			///@-`````````````````````````````````````````````
			//_________
			/*****************************/
			
			///_CHILD__=>LoadingView()
			/*****************************/
			// MARK: - Lottie animation json
			if isLoading {
				LoadingView()
			}
			
			if isSuccessful {
				SuccessView()
			}
			/*****************************/
			
		}//||END__PARENT-ZSTACK||
		
		//*****************************/
	}///-||End Of body||
	/*©-----------------------------------------©*/
}// END: [STRUCT]
