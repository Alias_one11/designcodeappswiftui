import SwiftUI

// MARK: - Preview
struct CoverView_Previews: PreviewProvider {
	
	static var previews: some View {
		
		CoverView()//.padding(.all, 100)
			//.preferredColorScheme(.dark)
			//.previewLayout(.sizeThatFits)
			.previewLayout(.fixed(width: 375, height: 780))
	}
}

struct CoverView: View {
	// MARK: - ©Global-PROPERTIES
	/*****************************/
	// MARK: - ©State-->PROPERTIES
	@State private var show: Bool = false
	@State private var viewState: CGSize = CGSize.zero
	@State private var isDragging: Bool = false
	/*****************************/
	
	/*©-----------------------------------------©*/
	var body: some View {
		
		//||_PARENT__=>VSTACK||
		//*****************************/
		VStack {
			//__________
			///_CHILD__=>GeometryReader
			/*****************************/
			GeometryReader { geometry in
				// MARK: - Header
				Text("Learn design & code.\nFrom scratch.")
					.font(.system(size: geometry.size.width / 10, weight: .bold))
					.foregroundColor(.white)
				
			}///END-GeometryReader
			.frame(maxWidth: 375, maxHeight: 100)
			.padding(.horizontal, 16)
			// - Makes the text move
			.offset(x: viewState.width / 15, y: viewState.height / 15)
			/*****************************/
			
			// MARK: - Sub-Headline
			Text("80 hours of courses for SwiftUI, React and design tools.").bold()
				.font(.subheadline)
				.frame(width: 250)
				// - Makes the text move
				.offset(x: viewState.width / 20, y: viewState.height / 20)
			
			// - Spacing horizontally
			Spacer()
			
		}///END-VStack
		.multilineTextAlignment(.center)
		.padding(.top, 100)
		.frame(height: 477)
		.frame(maxWidth: .infinity)
		//__________
		// MARK: - background image setup on the `Z` axis
		///@`````````````````````````````````````````````
		.background(
			///__________
			ZStack {
				// - blob image #1
				Image(uiImage: #imageLiteral(resourceName: "Blob"))
					.offset(x: -150, y: -200)
					.rotationEffect(Angle(degrees: show ? 360 + 90 : 90))
					.blendMode(.plusDarker)
					//__________
					// MARK: - animation with duration && onAppear
					///@`````````````````````````````````````````````
					.animation(
						Animation.linear(duration: 120)
							.repeatForever(autoreverses: false)
						//						nil
					)
					.onAppear {
						// - The animation will start, when the blob
						// - appears on the screen
						show = true
					}
				///@-`````````````````````````````````````````````
				
				// - blob image #2
				Image(uiImage: #imageLiteral(resourceName: "Blob"))
					.offset(x: -200, y: -250)
					.rotationEffect(Angle(degrees: show ? 360 : 0), anchor: .leading)
					.blendMode(.overlay)
					//__________
					// MARK: - animation with duration && onAppear
					///@`````````````````````````````````````````````
					.animation(
						Animation.linear(duration: 120)
							.repeatForever(autoreverses: false)
					)
				///@-`````````````````````````````````````````````
				//_________
			}//||END__PARENT-VSTACK||
			
		)///END-.background
		.background(
			Image(uiImage: #imageLiteral(resourceName: "Card3"))
				.offset(x: viewState.width / 25, y: viewState.height / 25)
			, alignment: .bottom
		)
		.background(Color(#colorLiteral(red: 0.4117647059, green: 0.4705882353, blue: 0.9725490196, alpha: 1)))
		///@-`````````````````````````````````````````````
		//_________
		// MARK: - clipShape corner radius
		///@`````````````````````````````````````````````
		.clipShape(
			RoundedRectangle(cornerRadius: 30, style: .continuous)
		)
		///@-`````````````````````````````````````````````
		// MARK: - 3d paralex effect, based on the drag gesture
		.scaleEffect(isDragging ? 0.9 : 1)
		.animation(
			.timingCurve(0.2, 0.8, 0.2, 1, duration: 0.8)
		)
		.rotation3DEffect(
			Angle(degrees: 5),
			axis: (x: viewState.width,
				   y: viewState.height,
				   z: 0.0)
		)
		///__________
		// MARK: - gesture
		///@`````````````````````````````````````````````
		.gesture(
			DragGesture().onChanged({ value in
				///__________
				viewState = value.translation
				isDragging = true
				
			})
			// - onEnded, when we are releasing
			.onEnded({ value in
				// - Reset the state/position
				viewState = .zero
				isDragging = false
			})
		)
		//*****************************/
		
	}///-||End Of body||
	/*©-----------------------------------------©*/
	
}// END: [STRUCT]
