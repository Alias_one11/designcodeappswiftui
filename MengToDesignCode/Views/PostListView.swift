import SwiftUI

// MARK: - Preview
struct PostListView_Previews: PreviewProvider {
	
	static var previews: some View {
		
		PostListView()//.padding(.all, 100)
			//.preferredColorScheme(.dark)
			//.previewLayout(.sizeThatFits)
			.previewLayout(.fixed(width: 540, height: 640))
	}
}

struct PostListView: View {
	// MARK: - ©Global-PROPERTIES
	/*****************************/
	@ObservedObject var store = DataStore()
	
	
	/*****************************/
	
	
	/**©---------------------------------------------------------------©*/
	// MARK: - Helper methods
	
	
	/*©-----------------------------------------©*/
	var body: some View {
		
		//||_PARENT__=>List||
		//*****************************/
		List(store.posts) { post in
			///_CHILD__=>VStack
			/*****************************/
			VStack(alignment: .leading, spacing: 8.0) {
				// MARK: - Title
				Text(post.title)
					.font(.system(.title, design: .serif)).bold()
				
				// MARK: - Body
				Text(post.body)
					.font(.system(.subheadline))
					.foregroundColor(.secondary)
			}///END-VStack
			/*****************************/
			
		}//||END__PARENT-List||
	//*****************************/
	
	
}///-||End Of body||
/*©-----------------------------------------©*/
}// END: [STRUCT]
