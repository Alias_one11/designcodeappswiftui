import SwiftUI
import SDWebImageSwiftUI

struct CourseView: View {
	/// MARK: - ©Global-PROPERTIES
	/*****************************/
	var course: Course
	var index: Int
	var bounds: GeometryProxy
	
	// MARK: - ©State-->PROPERTIES
	@Binding var isScrollable: Bool 
	
	// MARK: - ©Binding-->PROPERTIES
	@Binding var show: Bool
	@Binding var active: Bool
	@Binding var activeIndex: Int
	@Binding var activeView: CGSize
	
	// MARK: - ©State-->PROPERTIES
	
	let quote1: String = "Take your SwiftUI App to the App Store with" +
		" advanced techniques like API data, packages and CMS."
	
	let quote2: String = "This course is unlike any other. We care about design and want to make sure that you get better at it in the process. It was written for designers and developers who are passionate about collaborating and building real apps for iOS and macOS. While it's not one codebase for all apps, you learn once and can apply the techniques and controls to all platforms with incredible quality, consistency and performance. It's beginner-friendly, but it's also packed with design tricks and efficient workflows for building great user interfaces and interactions."
	
	let quote3: String = "Minimal coding experience required, such as in HTML and CSS. Please note that Xcode 11 and Catalina are essential. Once you get everything installed, it'll get a lot friendlier! I added a bunch of troubleshoots at the end of this page to help you navigate the issues you might encounter."
	
	/*****************************/
	
	
	/**©---------------------------------------------------------------©*/
	// MARK: - Helper methods
	
	
	/*©-----------------------------------------©*/
	var body: some View {
		///__________
		//_PARENT__=>VSTACK
		//*****************************/
		// - Aligning everything in the ZStack to the top
		ZStack(alignment: .top) {
			
			///_CHILD__=>VStack
			/*****************************/
			VStack(alignment: .leading, spacing: 30.0) {
				// MARK: - quotes #1
				Text(quote1)
				
				// MARK: - Title
				Text("About this course")
					.font(.title).bold()
				
				// MARK: - quotes #2
				Text(quote2)

				// MARK: - quotes #3
				Text(quote3)

			}///END-VStack
			.animation(nil)
			.padding(30)
			.frame(maxWidth: show ? .infinity : screen.width - 60,
				   maxHeight: show ? .infinity : 280, alignment: .top)
			.offset(y: show ? 460 : 0)
			// - Adds a card like feature to the text behind the card
			.background(Color("background1"))
			// - Corner radius
			.clipShape(
				RoundedRectangle(
					cornerRadius: show ? getCardCornerRadius(bounds: bounds) : 30,
					style: .continuous)
			)
			.shadow(color: Color.black.opacity(0.2),
					radius: 20, x: 0, y: 20)
			// - Adding an opacity animation
			.opacity(show ? 1 : 0)
			
			/*****************************/
			
			///_CHILD__=>VStack
			/*****************************/
			
			/*****************************/
			VStack {
				///_CHILD__=>HStack
				/*****************************/
				HStack(alignment: .top) {
					
					///_CHILD__=>Vstack
					/*****************************/
					VStack(alignment: .leading, spacing: 8.0) {
						
						// MARK: - course.title
						Text(course.title)
							.font(.system(size: 24, weight: .bold))
							.foregroundColor(.white)
						
						// MARK: - course.subtitle
						Text(course.subtitle)
							.foregroundColor(Color.white.opacity(0.7))
						
					}///END-VStack
					/*****************************/
					Spacer()/// - Spaced horizontally
					
					///_CHILD__=>ZStack
					/*****************************/
					// MARK: - Image logo
					ZStack {
						Image(uiImage: course.logo)
							// - Will make the logo disappears,
						    // - when in full screen
							.opacity(show ? 0 : 1)
						
						///_CHILD__=>VStack
						/*****************************/
						// MARK: - xmark.square.fill
						// - Once the logo image disappears,
						// - a close button will appear
						VStack {
							Image(systemName: "xmark.square.fill")
								.font(.system(size: 16, weight: .medium))
								.foregroundColor(.white)
							
							
						}///END-VStack
						.frame(width: 36, height: 36)
						.background(Color.black)
						.clipShape(Circle())
						// xmark Its set to invisible unless, the card is tapped
						.opacity(show ? 1 : 0)
						.offset(x: 2, y: -2)
						/*****************************/
						
					}///END-ZStack
					/*****************************/
					
				}///END-HStack
				/*****************************/
				Spacer()/// - Spaced vertically
				
				// MARK:- Main card image
				WebImage(url: course.image)
					.resizable()
					.aspectRatio(contentMode: .fit)
					.frame(maxWidth: .infinity)
					.frame(height: 140, alignment: .top)
				
			}///END-VStack
			// - Adjusted the padding at the top of the screen,
			// - when animating the screen transition from small to big
			.padding(show ? 30 : 20)
			.padding(.top, show ? 30 : 0)
			// - Will make the card when tapped, shown full screen
			//		.frame(width: show ? screen.width : screen.width - 60,
			//			   height: show ? screen.height : 280)
			// - Will make the card when tapped, shown full screen all the
			// - way to the top of the screen and not stop right before it
			// - NOTE!! must add .edgesIgnoringSafeArea(.all)
			// - as the last modifier
			.frame(maxWidth: show ? .infinity : screen.width - 60,
				   maxHeight: show ? 460 : 280)
			.background(Color(course.color))
			// - Adding a corner radius
			.clipShape(RoundedRectangle(
						cornerRadius: show ? getCardCornerRadius(bounds: bounds) : 30,
						style: .continuous)
			)
			.shadow(color: Color(course.color).opacity(0.3), radius: 20, x: 0, y: 20)
			//__________
			// MARK: - DragGesture animation
			///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
			.gesture( show ?// <--Ternary
				DragGesture().onChanged({ value in
					// - Will limit how small the card can get when you keep dragging
					guard value.translation.height < 300 else { return }
					// - Prevents dragging up to scale the card
					guard value.translation.height > 0 else { return }

					// - |.translation|: The total translation from the start of
					// - the drag gesture to the current event of the drag gesture.
					activeView = value.translation
				})
				.onEnded({ _ in
					// - Resets all the states from the properties
					if activeView.height > 50 {
						show = false
						active = false
						activeIndex = -1
						isScrollable = false
					}
					// - Resets the drag position, after the card is released
					activeView = .zero
				})
				: nil // else nil
			)//_END__=>.gesture
			///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
			//_________
			//__________
			// MARK: - onTap
			///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
			.onTapGesture {
				///__________
				show.toggle()
				active.toggle()
				
				if show {
					// Only passes the correct index if that card selected is active
					activeIndex = index
				} else {
					activeIndex = -1
				}
				
				DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) {
					///__________
					isScrollable = true
				}
			}
			///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
			//_________
			//__________
			// MARK: - Layering a detail course view on top of this one
			///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
			// - Inside the Parent ZStack
			if isScrollable {
				CourseDetailView(course: course,
								 bounds: bounds,
								 show: $show,
								 active: $active,
								 activeIndex: $activeIndex,
								 isScrollable: $isScrollable)
				// - Sets this screen to a white background when
				// - the CourseDetailView screen takes over
					.background(Color.white)
					.clipShape(
						RoundedRectangle(
							cornerRadius: show ? getCardCornerRadius(bounds: bounds) : 30,
							style: .continuous)
					)
					.animation(nil)
					.transition(.identity)
			}
			///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
			//_________
		}//END__PARENT-ZSTACK
		.frame(height: show ?
				bounds.size.height +
				bounds.safeAreaInsets.top +
				bounds.safeAreaInsets.bottom : 280)
		///*------------------------------*/
		//__________
		// MARK: - Effects, rotations & hue color changes when the card is dragged
		///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		.scaleEffect(1 - activeView.height / 1000)
		.rotation3DEffect(Angle(degrees: Double(activeView.height / 10)),
						  axis: (x: 0.0, y: 10.0, z: 0.0))
		// - |.hueRotation|: Use hue rotation effect to shift all of
		// - the colors in a view according to the angle you specify.
		.hueRotation(Angle(degrees: Double(activeView.height)))
		///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		//_________
		//__________
		// MARK: - Animation
		// Provides a smooth frame animation when the card is tapped
		///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		.animation(
			.spring(response: 0.5, dampingFraction: 0.7, blendDuration: 0)
		)
		///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		//_________
		//__________
		// MARK: - DragGesture animation
		///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		.gesture( show ?// <--Ternary
					DragGesture().onChanged({ value in
						// - Will limit how small the card can get when you keep dragging
						guard value.translation.height < 300 else { return }
						// - Prevents dragging up to scale the card
						guard value.translation.height > 50 else { return }
						
						// - |.translation|: The total translation from the start of
						// - the drag gesture to the current event of the drag gesture.
						activeView = value.translation
					})
					.onEnded({ _ in
						// - Resets all the states from the properties
						if activeView.height > 50 {
							show = false
							active = false
							activeIndex = -1
							isScrollable = false
						}
						// - Resets the drag position, after the card is released
						activeView = .zero
					})
					: nil // else nil
		)//_END__=>.gesture
		///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		.disabled(active && !isScrollable ? true : false)
		.edgesIgnoringSafeArea(.all)
		///__________
		//*****************************/

	}/*©-----------------------------------------©*/
}// END: [STRUCT]
