import SwiftUI
import Combine
import SDWebImageSwiftUI
import SwiftUI

// MARK: - Preview
struct CourseListView_Previews: PreviewProvider {
	
	static var previews: some View {
		
		CourseListView()//.padding(.all, 100)
			//.preferredColorScheme(.dark)
			//.previewLayout(.sizeThatFits)
			.previewLayout(.fixed(width: 375, height: 780))
	}
}

struct CourseListView: View {
	/// MARK: - ©Global-PROPERTIES
	/*****************************/
	// MARK: - ©ObservedObject-->PROPERTIES
	@ObservedObject var store: CourseStore = CourseStore()
	
	// MARK: - ©State-->PROPERTIES
	@State private var active: Bool = false
	@State private var activeIndex: Int = -1
	@State private var activeView: CGSize = CGSize.zero
	@State private var isScrollable: Bool = false
	

	
	// MARK: - ©Environment-->PROPERTIES
	@Environment(\.horizontalSizeClass) var horizontalSizeClass
	/*****************************/
	
	/*©-----------------------------------------©*/
	var body: some View {
		
		//||_PARENT__=>GeometryReader||
		//*****************************/
		GeometryReader { bounds in
			///_CHILD__=>ZStack
			/*****************************/
			//_________
			ZStack {
				///_CHILD__=>Color
				/*****************************/
				// - Gray color when transitioning through animation
				// - when the card is pressed. Froma light gray to
				// - a darker gray as the card scales down in size
				Color.black.opacity(Double(activeView.height / 500))
					.animation(.linear)
					.edgesIgnoringSafeArea(.all)
				/*****************************/
				
				///_CHILD__=>ScrollView
				/*****************************/
				ScrollView {
					///__________
					VStack(spacing: 30.0) {
						// MARK: - Title
						Text("Courses")
							.font(.largeTitle).bold()
							.frame(maxWidth: .infinity, alignment: .leading)
							.padding(.leading, 30)
							.padding(.top, 30)
							// - Blurs the text when a card is tapped!
							.blur(radius: active ? 20 : 0)
						
						
						///_CHILD__=>GeometryReader
						/*****************************/
						ForEach(store.courses.indices, id: \.self) { index in
							///__________
							GeometryReader { geometry in
								// - Now it shouldnt matter which card you tap,
								// - the state changes for both cards
								// - Since your bound to a single state,
								// - at the CourseView() level
								// MARK: - CourseView()
								CourseView(course: store.courses[index],
										   index: index, bounds: bounds,
										   isScrollable: $isScrollable,
										   show: $store.courses[index].show,
										   active: $active,
										   activeIndex: $activeIndex,
										   activeView: $activeView)
									// - Fills the gap or else do not change anything
									// - When the bottom card is pressed it will move up
									// - to where the top card is
									.offset(y: store.courses[index].show ? -geometry.frame(in: .global).minY : 0)
									// - If not the active card the opacity & scale effect will
									// - scale down those known active cards
									.opacity(activeIndex != index && active ? 0 : 1)
									.scaleEffect(activeIndex != index && active ? 0.5 : 1)
									// - If not the active card, those cards will dissapear to the right
									.offset(x: activeIndex != index && active ? bounds.size.width : 0)
								
							}///END-GeometryReader
							.frame(height: horizontalSizeClass == .regular ? 80 : 280)
							// - Centers card #2
							.frame(maxWidth: store.courses[index].show ?
									712 : getCardWidth(bounds: bounds))
							// - When a card is tapped, any card, that card will take up
							// - all the space on the screen when that sepcific card is tapped.
							.zIndex(store.courses[index].show ? 1 : 0)
							
						}///END-ForEach
						/*****************************/
						
						
						/*****************************/
					}///END-VStack
					.frame(width: bounds.size.width)
					// - Helps giving the cards a proper animation
					.animation(
						.spring(response: 0.5,
								dampingFraction: 0.6,
								blendDuration: 0)
					)
					///*------------------------------*/
					
				}///END-ScrowView
				// - To show or hide the status bar.
				.statusBar(hidden: active ? true : false)
				// - .linear provides a fade in, fade out animation
				.animation(.linear)
				.disabled(active && !isScrollable ? true : false)
				/*****************************/
				
			}///END-ZStack
			
			/*****************************/
			
		}//||END__PARENT-GeometryReader||
		//*****************************/
		
	}///-||End Of body||
	/*©-----------------------------------------©*/
}// END: [STRUCT]

func getCardWidth(bounds: GeometryProxy) -> CGFloat {
	///__________
	let boundsWidth: CGFloat = 712
	
	if bounds.size.width > boundsWidth {
		return boundsWidth
	}
	
	return bounds.size.width - 60
}

func getCardCornerRadius(bounds: GeometryProxy) -> CGFloat {
	///__________
	let defaultBounds: CGFloat = 30
	let boundsWidth: CGFloat = 712
	let boundsTop: CGFloat = 44
	
	if bounds.size.width < boundsWidth &&
		bounds.safeAreaInsets.top < boundsTop {
		///__________
		return 0 // As a corner radius
	}
	
	return defaultBounds
}
