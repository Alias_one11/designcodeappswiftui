import SwiftUI
import SDWebImageSwiftUI

// MARK: - Preview
struct CourseDetailView_Previews: PreviewProvider {
	
	static var previews: some View {
		
		GeometryReader { bounds in
			CourseDetailView(course: courseData[0],
							 bounds: bounds,
							 show: .constant(true),
							 active: .constant(true),
							 activeIndex: .constant(-1),
							 isScrollable: .constant(true))
				//.padding(.all, 100)
				//.preferredColorScheme(.dark)
				//.previewLayout(.sizeThatFits)
				.previewLayout(.fixed(width: 540, height: 640))
		}
	}
}

struct CourseDetailView: View {
	// MARK: - ©Global-PROPERTIES
	/*****************************/
	var course: Course
	var bounds: GeometryProxy
	
	// MARK: - ©Binding-->PROPERTIES
	@Binding var show: Bool
	@Binding var active: Bool
	@Binding var activeIndex: Int
	@Binding var isScrollable: Bool
	
	let quote1: String = "Take your SwiftUI App to the App Store with" +
		" advanced techniques like API data, packages and CMS."
	
	let quote2: String = "This course is unlike any other. We care about design and want to make sure that you get better at it in the process. It was written for designers and developers who are passionate about collaborating and building real apps for iOS and macOS. While it's not one codebase for all apps, you learn once and can apply the techniques and controls to all platforms with incredible quality, consistency and performance. It's beginner-friendly, but it's also packed with design tricks and efficient workflows for building great user interfaces and interactions."
	
	let quote3: String = "Minimal coding experience required, such as in HTML and CSS. Please note that Xcode 11 and Catalina are essential. Once you get everything installed, it'll get a lot friendlier! I added a bunch of troubleshoots at the end of this page to help you navigate the issues you might encounter."
	/*****************************/
	
	
	/**©---------------------------------------------------------------©*/
	// MARK: - Helper methods
	
	
	/*©-----------------------------------------©*/
	var body: some View {
		
		//||_PARENT__=>ScrollView||
		//*****************************/
		ScrollView {
			///_CHILD__=>VStack
			/*****************************/
			VStack(spacing: 0) {
				///_CHILD__=>VStack
				/*****************************/
				VStack {
					///_CHILD__=>HStack
					/*****************************/
					HStack(alignment: .top) {
						
						///_CHILD__=>Vstack
						/*****************************/
						VStack(alignment: .leading, spacing: 8.0) {
							
							// MARK: - course.title
							Text(course.title)
								.font(.system(size: 24, weight: .bold))
								.foregroundColor(.white)
							
							// MARK: - course.subtitle
							Text(course.subtitle)
								.foregroundColor(Color.white.opacity(0.7))
							
						}///END-VStack
						/*****************************/
						Spacer()/// - Spaced horizontally
						
						///_CHILD__=>ZStack
						/*****************************/
						// MARK: - Image logo
						ZStack {
							
							///_CHILD__=>VStack
							/*****************************/
							// MARK: - xmark.square.fill
							// - Once the logo image disappears,
							// - a close button will appear
							VStack {
								Image(systemName: "xmark.square.fill")
									.font(.system(size: 16, weight: .medium))
									.foregroundColor(.white)
								
								
							}///END-VStack
							.frame(width: 36, height: 36)
							.background(Color.black)
							.clipShape(Circle())
							//__________
							// MARK: - onTap closes the view from the close button only
							///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
							.onTapGesture {
								show = false
								active = false
								activeIndex = -1
								isScrollable = false
							}
							///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
							//_________
							/*****************************/
							
						}///END-ZStack
						/*****************************/
						
					}///END-HStack
					/*****************************/
					Spacer()/// - Spaced vertically
					
					// MARK:- Main card image
					WebImage(url: course.image)
						.resizable()
						.aspectRatio(contentMode: .fit)
						.frame(maxWidth: .infinity)
						.frame(height: 140, alignment: .top)
					
				}///END-VStack
				// - Adjusted the padding at the top of the screen,
				// - when animating the screen transition from small to big
				.padding(show ? 30 : 20)
				.padding(.top, show ? 30 : 0)
				// - Will make the card when tapped, shown full screen
				//		.frame(width: show ? screen.width : screen.width - 60,
				//			   height: show ? screen.height : 280)
				// - Will make the card when tapped, shown full screen all the
				// - way to the top of the screen and not stop right before it
				// - NOTE!! must add .edgesIgnoringSafeArea(.all)
				// - as the last modifier
				.frame(maxWidth: show ? .infinity : bounds.size.width - 60)
				.frame(height: show ? 460 : 280)
				.background(Color(course.color))
				// - Adding a corner radius
				.clipShape(
					RoundedRectangle(
						cornerRadius: getCardCornerRadius(bounds: bounds),
											style: .continuous)
				)
				.shadow(color: Color(course.color).opacity(0.3), radius: 20, x: 0, y: 20)
				/*****************************/
				
				///_CHILD__=>VStack
				/*****************************/
				VStack(alignment: .leading, spacing: 30.0) {
					// MARK: - quotes #1
					Text(quote1)
					
					// MARK: - Title
					Text("About this course")
						.font(.title).bold()
					
					// MARK: - quotes #2
					Text(quote2)
					
					// MARK: - quotes #3
					Text(quote3)
					
				}///END-VStack
				.padding(30)
			}///END-VStack
			/*****************************/
		}//||END__PARENT-ScrollView||
		.edgesIgnoringSafeArea(.all)
		//*****************************/
	}///-||End Of body||
	/*©-----------------------------------------©*/
}// END: [STRUCT]
