import SwiftUI

struct MainView_Previews: PreviewProvider {
	
	static var previews: some View {
		
		MainView()//.padding(.all, 100)
		//.preferredColorScheme(.dark)
		//.previewLayout(.sizeThatFits)
		//		.previewLayout(.fixed(width: 440, height: 940))
			.environmentObject(UserStore())
	}
}

/// - Where all the main views come together
struct MainView: View {
	/// - ©Global-PROPERTIES
	/*****************************/
	// MARK: - ©State-PROPERTIES
	@State private var showProfile: Bool = false
	@State private var viewState: CGSize = CGSize.zero
	@State private var showContent: Bool = false
	
	// MARK: - ©EnvironmentObject-->PROPERTIES
	@EnvironmentObject var user: UserStore
	/*****************************/
	
	/**©---------------------------------------------------------------©*/
	// MARK: - Helper methods
	
	
	/*©-----------------------------------------©*/
	var body: some View {
		
		ZStack {
			// ROUTE BACKGROUND
			Color("background2")
				.edgesIgnoringSafeArea(.all)
			//__________
			///_CHILD__=>HomeBackgroundView()
			/**********************************/
			HomeBackgroundView(showProfile: $showProfile)
				.offset(y: showProfile ? -450 : 0)
				
				// MARK: - Animation & effects
				///@`````````````````````````````````````````````
				// Adding 3d effect with the background view & the MenuView card
				.rotation3DEffect(
					Angle(
						degrees: showProfile ?
							Double(viewState.height / 10) - 10 : 0
					), axis: (x: 10.0, y: 0.0, z: 0.0))
				.scaleEffect(showProfile ? 0.9 : 1)
				.animation(
					.spring(response: 0.5, dampingFraction: 0.6,
							blendDuration: 0)
				)
				///@-`````````````````````````````````````````````
				.edgesIgnoringSafeArea(.all)
			/**********************************/
			
			// MARK: - HomeView()
			///_CHILD__=>TabView
			/*****************************/
			//_________
//			TabView {
				HomeView(showProfile: $showProfile,
						 showContent: $showContent,
						 viewState: $viewState)
			
//					//__________
//					// MARK: - tabItem
//					///@`````````````````````````````````````````````
//					.tabItem {
//						Image(systemName: "play.circle.fill")
//						Text("Home")
//					}
//					///@-`````````````````````````````````````````````
//					//_________
//			}///END-VStack
			
			/*****************************/
			
				
			// MARK: - MenuView()
			/**©------------------------------------------------------------------©*/
			MenuView(showProfile: $showProfile)
				.background(Color.black.opacity(0.001))
				// Will show profile if its active/button tapped
				.offset(y: showProfile ? 0 : screen.height)
				.offset(y: viewState.height)
				///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
				.animation(
					.spring(response: 0.5, dampingFraction: 0.6,
							blendDuration: 0)
				)
				// - Will toggle the background when the image
				// - button is clicked
				.onTapGesture {
					showProfile.toggle()
				}
				// - Allows us to drag the card from the MenuView when it pops up
				.gesture(
					DragGesture().onChanged({ (value) in
						/// - |.translation|
						/// - The total translation from the start of the drag
						/// - gesture to the current event of the drag gesture.
						viewState = value.translation
					})
					.onEnded({ (value) in
						///|__________|
						// - If the view height is greate then 50, the profile dismisses
						if viewState.height > 50 { showProfile = false }
						// - Resetting the view state
						viewState = .zero
					})
				)
			// - Going from the MainView --> to the ContentView
			///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
			///_CHILD__=>LoginView()
			/*****************************/
			if user.showLogin {
				///__________
				///_CHILD__=>ZStack
				/*****************************/
				ZStack {
					///_CHILD__=>LoginView()<--ZStack
					/**********************************/
					//_________
					LoginView()
					
					/**********************************/
				
					// - Creating a close button to close out the ContentView
					VStack {
						///__________
						HStack {
							Spacer()
							///__________
							Image(systemName: "xmark.square.fill")
								.frame(width: 36, height: 36)
								.foregroundColor(.white)
								.background(Color.black)
								.clipShape(Circle())
						}
						Spacer()
						///__________
						
					}///END-VStack
					.padding()
					.onTapGesture(perform: {
						///__________
						user.showLogin = false
					})
					///*------------------------------*/
				}//END-ZStack
				/*****************************/
				
			}///END-if statement
			/*****************************/
			if showContent {
				// - Ignoring the safe area & hiding the view behind
				// - the ContentView, so it cannot be seen when the
				// - progress bar view is pressed!
				BlurView(style: .systemMaterial).edgesIgnoringSafeArea(.all)
				
				ContentView()
				
				///_CHILD-->PARENT__VStack=>HStack
				/*****************************/
				// - Creating a close button to close out the ContentView
				VStack {
					///__________
					HStack {
						Spacer()
						///__________
						Image(systemName: "xmark.square.fill")
							.frame(width: 36, height: 36)
							.foregroundColor(.white)
							.background(Color.black)
							.clipShape(Circle())
					}
					Spacer()
					///__________
				}
				.offset(x: -16, y: 16)
				// - Gesture to finally close the content view
				.transition(.move(edge: .top))
				// - The close x mark will animate slowly from above
				.animation(.spring(response: 0.7, dampingFraction: 0.8, blendDuration: 0))
				.onTapGesture {
					showContent = false
				}
				/*****************************/
			}//_END__=>if Statement
			///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
			
				///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		}///_END__=>ZSTACK
	}
	/*©-----------------------------------------©*/
}// END: [STRUCT]

// MARK: - COMPONENTS
///*©-----------------------------------------©*/
struct HomeBackgroundView: View {
	// MARK: - ©PROPERTIES
	@Binding var showProfile: Bool
	
	var body: some View {
		VStack {
			///__________
			LinearGradient(
				gradient:
					Gradient(colors: [
						Color("background2"),
						Color("background1")
					]),
				startPoint: .top, endPoint: .bottom)
				.frame(height: 200)
			
			Spacer()/// Spaced vertically
			
		}///END-VStack
		.background(Color("background1"))
		.clipShape(
			RoundedRectangle(cornerRadius: showProfile ? 30 : 0,
							 style: .continuous)
		)
		.shadow(color: Color.black.opacity(0.2), radius: 20, x: 0, y: 20)
		///*------------------------------*/
	}
}
