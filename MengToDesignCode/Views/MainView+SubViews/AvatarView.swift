import SwiftUI

// MARK: - Preview
struct AvatarView_Previews: PreviewProvider {
	static var previews: some View {
		AvatarView(showProfile: .constant(true))
			.previewLayout(.fixed(width: 420, height: 420))
			.environmentObject(UserStore())
	}
}

// MARK: - COMPONENTS
///*©-----------------------------------------©*/
struct AvatarView: View {
	// MARK: - ©Global-PROPERTIES
	/*****************************/
	@Binding var showProfile: Bool
	@EnvironmentObject var user: UserStore
	/*****************************/
	
	/*©-----------------------------------------©*/
	var body: some View {
		
		//||_PARENT__=>VSTACK||
		//*****************************/
		VStack {
			/// - If user is logged in, show profile image
			if user.isLoggedIn {
				Button(action: { showProfile.toggle() }, label: {
					Image("babygirl")
						// Removes annoying tint over image
						.renderingMode(.original).resizable()
						.frame(width: 36, height: 36)
						.clipShape(Circle())
						///*------------------------------*/
				})
			} else {
				// - If user is not logged in, show person sfsymbol image
				Button(action: { user.showLogin.toggle() }, label: {
					Image(systemName: "person")
						// Removes annoying tint over image
						.foregroundColor(.primary)
						.font(.system(size: 16, weight: .medium))
						.frame(width: 36, height: 36)
						.background(Color("background3"))
						// - Must clip the image before adding shadow
						.clipShape(Circle())
						// - Custom shadow
						.shadow(color: Color.black.opacity(0.1),
								radius: 1, x: 0, y: 1)
						.shadow(color: Color.black.opacity(0.2),
								radius: 10, x: 0, y: 10)
						///*------------------------------*/
				})
			}
		}//||END__PARENT-VSTACK||
		//*****************************/
		
	}///-||End Of body||
	/*©-----------------------------------------©*/
}// END: [STRUCT]
