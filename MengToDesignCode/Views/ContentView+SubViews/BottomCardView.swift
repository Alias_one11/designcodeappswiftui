import SwiftUI


struct BottomCardView_Previews: PreviewProvider {
	static var previews: some View {
		BottomCardView(show: .constant(true))
			.previewLayout(.fixed(width: 540, height: 640))
	}
}

// MARK: - COMPONENTS
///*©-----------------------------------------©*/
struct BottomCardView: View {
	// MARK: - ©Global-PROPERTIES
	/*****************************/
	let description = "This certificate is proof that Alias111 " +
		"is the baddest man on the planet! Beware, no drama over " +
		"here! I am a Ios legend! Still living."
	
	// MARK: - ©Binding-->PROPERTIES
	/// - The property has to be of type @Binding
	/// - to be able to work outside of the object
	@Binding var show: Bool
	
	/*****************************/
	
	var body: some View {
		VStack(spacing: 20) {
			// Gives the stack a little notch at the top of the view
			Rectangle().frame(width: 40, height: 5).cornerRadius(3)
				.opacity(0.1)
			
			Text(description)
				.fontWeight(.semibold)
				.multilineTextAlignment(.center)
				.font(.subheadline)
				.lineSpacing(4)
			
			HStack(spacing: 20.0) {
				// MARK: - ©RingView()
				RingView(color1: #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1), color2: #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1), width: 88,
						 height: 88, percent: 78, show: $show)
				.animation(Animation.easeInOut.delay(0.3))
				///*------------------------------*/
				
				///_CHILD__=>VStack
				/*****************************/
				VStack(alignment: .leading, spacing: 8.0) {
					Text("SwiftUI")
						.fontWeight(.bold)
					
					Text("12 of 12 sections completed" +
							"\n10 hours spent so far")
						.font(.footnote)
						.foregroundColor(.gray)
						.lineSpacing(4)
					
				}
				.padding(20)
				.background(Color("background3"))
				.cornerRadius(20)
				.shadow(color: Color.black.opacity(0.2), radius: 20, x: 0, y: 10)
				/*****************************/

				
			}///_END__=HStack
			///*------------------------------*/
			
			Spacer()
			///|__________|
		}//_END__>VStack
		.padding(.top, 8).padding(.horizontal, 20)
		.frame(maxWidth: 712)
		.background(BlurView(style: .systemMaterial))
		.cornerRadius(30).shadow(radius: 20)
		// - Aligns the container for the view in the middle of the screen
		.frame(maxWidth: .infinity)
		///*------------------------------*/
		
	}
}// END: [STRUCT]

