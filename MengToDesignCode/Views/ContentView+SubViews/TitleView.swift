import SwiftUI

// MARK: - Preview
struct TitleView_Previews: PreviewProvider {
	
	static var previews: some View {
		
		TitleView()//.padding(.all, 100)
			//.preferredColorScheme(.dark)
			//.previewLayout(.sizeThatFits)
			.previewLayout(.fixed(width: 375, height: 780))
	}
}

// MARK: - COMPONENTS
///*©-----------------------------------------©*/
struct TitleView: View {
	// MARK: - ©Global-PROPERTIES
	/*****************************/
	
	/*****************************/
	
	/*©-----------------------------------------©*/
	var body: some View {
		
		//||_PARENT__=>VSTACK||
		//*****************************/
		VStack {
			///_CHILD__=>HStack
			/*****************************/
			HStack {
				///|__________|
				Text("Certificates").font(.largeTitle)
					.fontWeight(.bold)
				Spacer()
				
			}///_END__>HSTACK
			.padding()
			/*****************************/
			
			///_CHILD__=>Image<--VStack
			/**********************************/
			//_________
			Image("Background1")
				.resizable()
				.aspectRatio(contentMode: .fit)
				// - Keeps the image the correct width on any device
				.frame(maxWidth: 375)
			Spacer()/// Spaced vertically
			/**********************************/
		}//||END__PARENT-VSTACK||
		//*****************************/
	}///-||End Of body||
	/*©-----------------------------------------©*/
}// END: [STRUCT]
