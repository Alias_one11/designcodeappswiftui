import SwiftUI

// MARK: - Preview
struct CardView_Previews: PreviewProvider {
	static var previews: some View {
		CardView()
			.previewLayout(.fixed(width: 375, height: 480))
	}
}

// MARK: - COMPONENTS
///*©-----------------------------------------©*/
struct CardView: View {
	// MARK: - ©Global-PROPERTIES
	/*****************************/
	
	/*****************************/
	
	/*©-----------------------------------------©*/
	var body: some View {
		
		//||_PARENT__=>VSTACK||
		//*****************************/
		VStack {
			///_CHILD__=>HStack
			/*****************************/
			//_________
			HStack {
				///_CHILD__=>VStack
				/*****************************/
				//_________
				VStack(alignment: .leading) {
					// - UI DESIGN
					Text("UI Design").font(.title).fontWeight(.semibold)
						.foregroundColor(.white)
					
					// - CERTIFICATE
					Text("Certificate").foregroundColor(Color("accent"))
					
				}///_END__>VSTACK
				/*****************************/
				
				// in-HSTACK- Logo1 IMAGE
				Spacer()/// Spaced horizontally
				Image("Logo1")
				
				
			}///_END__>HSTACK
			.padding()
			/*****************************/
			
			Spacer()
			// in-VSTACK- Card5 IMAGE
			Image("Card1").resizable().aspectRatio(contentMode: .fill)
				.frame(width: 300, height: 110, alignment: .top)
			
		}//||END__PARENT-VSTACK||
		//*****************************/
		
	}///-||End Of body||
	/*©-----------------------------------------©*/
}// END: [STRUCT]
