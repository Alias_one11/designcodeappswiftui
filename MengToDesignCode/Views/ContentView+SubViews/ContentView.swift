import SwiftUI

struct ContentView: View {
	// MARK: - ©Global-PROPERTIES
	/*****************************/
	// x & y - axis set to (0,0) +
	@State private var viewState: CGSize = CGSize.zero
	@State private var showCard: Bool = false
	@State private var bottomState: CGSize = CGSize.zero
	@State private var showFullCard: Bool = false
	@State private var show: Bool = false
	/*****************************/
	
	/**©---------------------------------------------------------------©*/
	// MARK: - Helper methods
	
	
	/*©-----------------------------------------©*/
	var body: some View {
		///|__________|
		ZStack {
			// TITLE_VIEW
			TitleView().blur(radius: show ? 20 : 0)
				// - Sets the transparency of the title, to a lighter color when the bottom
				// - card is shown & back to its normal state when the card goes away.
				.opacity(showCard ? 0.4 : 1)
				// - Will offset the background, when the card is shown.
				.offset(y: showCard ? -200 : 0)
				// - Adding the value type(Enum.foo.nextfoo) to the `enums` used(.foo),
				// - can give you access to more functionality
				// - Now the animation will transition better & not instantly with `.delay`
				.animation(Animation.default.delay(0.1))
			
			// - BACKCARD_VIEW #1
			BackCardView()
				.frame(maxWidth: showCard ? 300 : 340)
				.frame(height: 200)
				.background(show ? Color("card3") : Color("card4"))
				.cornerRadius(20).shadow(radius: 20)
				.offset(y: show ? -400 : -40)
				// Will adjust the state on the view, when dragging the card!
				.offset(x: viewState.width, y: viewState.height)
				// Moves card #2 when the top card is tapped
				.offset(y: showCard ? -180 : 0)
				// 0.9 or 90% effect
				.scaleEffect(showCard ? 1 : 0.9)
				///|__________|
				.rotationEffect(.degrees(show ? 0 : 10))
				.rotationEffect(Angle(degrees: showCard ? -10 : 0))
//				.rotation3DEffect(Angle(degrees: showCard ? 0 : 10),
//								  axis: (x: 10, y: 0, z: 0)
//				)
				///|__________|
				.blendMode(.hardLight)
				/// animation
				///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
				.animation(.easeInOut(duration: 0.6)
				)
			///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
			
			// - BACKCARD_VIEW #2
			BackCardView()
				.frame(maxWidth: 340)
				.frame(height: 220)
				.background(show ? Color("card4") : Color("card3"))
				.cornerRadius(20).shadow(radius: 20)
				.offset(y: show ? -200 : -20)
				// Will adjust the state on the view, when dragging the card!
				.offset(x: viewState.width, y: viewState.height)
				// Moves card #2 when the top card is tapped
				.offset(y: showCard ? -140 : 0)
				// 0.95 or 95% effect
				.scaleEffect(showCard ? 1 : 0.95)
				///|__________|
				.rotationEffect(.degrees(show ? 0 : 5))
				.rotationEffect(Angle(degrees: showCard ? -5 : 0))
//				.rotation3DEffect(Angle(degrees: showCard ? 0 : 5), axis: (x: 10, y: 0, z: 0))
				///|__________|
				// Creates a transparent, with color type fill color
				.blendMode(.hardLight)
				/// animation
				///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
				.animation(.easeInOut(duration: 0.3)
				)
			///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
			
			// - CARD_VIEW
			CardView()
				.frame(maxWidth: showCard ? 375 : 340)
				.frame(height: 220)
				.background(Color.black)
				//				.cornerRadius(20)
				///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
				.clipShape(RoundedRectangle(
					cornerRadius: showCard ? 30 : 20,
					style: .continuous
				))
				///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
				.shadow(radius: 20)
				///|     ⇪     |
				// - Will adjust the state on the view, when dragging the card!
				.offset(x: viewState.width, y: viewState.height)
				// - Changes the state of the card, makes the card wider
				// - & moves it up, when the card is tapped.
				
				.offset(y: showCard ? -100 : 0)
				.blendMode(.hardLight)
				// - .spring() adds a smooth transition when releasing the card. With its
				// - parameters, it can add a lot less lag to the transitions & will handle
				// - the animation a lot faster.
				.animation(
					.spring(response: 0.3, dampingFraction: 0.6)
				)
				
				///|     DRAG-GESTURE     |
				///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
				.onTapGesture {
					showCard.toggle()
				}
				.gesture(
					/// - |DragGesture|
					/// - A dragging motion that invokes an action as
					/// - the drag-event sequence changes.
					DragGesture().onChanged({ (value) in
						/// - |translation: CGSize|
						/// - The total translation from the start of the drag
						/// - gesture to the current event of the drag gesture.
						viewState = value.translation
						// Blurs the background when selecting & dragging the cards
						show = true
					})
					/// - |.onEnded|
					/// - Adds an action to perform when the gesture ends.
					.onEnded({ (value) in
						// Resetting the card back to position (0,0) in the view(Start-Point)
						viewState = .zero
						// Resetting the card to show its original state
						show = false
					})
				)
			///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
			/*_____________________*/
			
			// Text to test the height amount on the bottom card
//			Text("\(bottomState.height)").offset(y: -300)
			
			// MARK: - @BottomCardView()
			GeometryReader { bounds in
				BottomCardView(show: $showCard)
					.blur(radius: show ? 20 : 0)
					// Will pop the card up when the card is tapped & go away when tapped again
					.offset(x: 0, y: showCard ? bounds.size.height / 2 :
								bounds.size.height +
								bounds.safeAreaInsets.top +
								bounds.safeAreaInsets.bottom
					)
					.offset(y: bottomState.height)
					///|__________|
					///|     ANIMATION     |
					///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
					/// - |.timingCurve|
					/// - Will handle the animation curve smoothly, depending on settings
					/// - Refer to https://cubic-bezier.com for demo
					.animation(.timingCurve(0.2, 0.8, 0.2, 1, duration: 0.8)
					)
					///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
					///|__________|
					///|     DRAG-GESTURE     |
					///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
					// Makes the bottom card draggable
					.gesture(
						DragGesture().onChanged({ (value) in
							bottomState = value.translation
							
							if showFullCard {
								bottomState.height += -300
							}
							// - Will keep it at -300 height if less then that
							// - value - & not continue up in the screen.
							if bottomState.height < -300 { bottomState.height = -300 }
						})
						.onEnded({ (value) in
							// - If the bottom card is above 50, itll dismiss the card
							if bottomState.height > 50 {
								showCard = false
								// !showFullCard: will reset the card to a decent point
							} else if (bottomState.height < -100 && !showFullCard) ||
										(bottomState.height < -250 && showFullCard) {
								// - Clips the card to this height when it reaches it & remains
								bottomState.height = -300
								showFullCard = true
							} else {
								// Resets the state
								bottomState = .zero
								showFullCard = false
							}
						})
				)
			}
//			.edgesIgnoringSafeArea(.all)
			///*------------------------------*/
			/*_____________________*/
			
			
		}///_END__>ZSTACK
		/*_____________________*/
		
	}
	/*©-----------------------------------------©*/
}// END: [STRUCT]

struct ContentView_Previews: PreviewProvider {
	
	static var previews: some View {
		
		ContentView()
		//.previewLayout(.sizeThatFits)
		//.previewLayout(.fixed(width: 540, height: 640))
	}
}
