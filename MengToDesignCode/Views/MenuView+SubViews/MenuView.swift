import SwiftUI

// MARK: - Preview
struct MenuView_Previews: PreviewProvider {
	
	static var previews: some View {
		
		MenuView(showProfile: .constant(true))//.padding(.all, 100)
			//.preferredColorScheme(.dark)
			//.previewLayout(.sizeThatFits)
			.previewLayout(.fixed(width: 375, height: 480))
			.environmentObject(UserStore())
	}
}

struct MenuView: View {
	/// MARK: - ©Global-PROPERTIES
	/*****************************/
	// MARK: - ©EnvironmentObject-->PROPERTIES
	@EnvironmentObject var user: UserStore
	
	// MARK: - ©Binding-->PROPERTIES
	@Binding var showProfile: Bool
	
	// MARK: - ©State-->PROPERTIES
	@State private var isShowingAlert: Bool = false
	/*****************************/
	
	/*©-----------------------------------------©*/
	var body: some View {
		
		//||_PARENT__=>VSTACK||
		//*****************************/
		VStack {
			Spacer()
			///|__________|
			VStack(spacing: 16) {
				
				///_CHILD__=>Text && Color.white<--VStack
				/**********************************/
				//_________
				// - Text under the image in the stack
				Text("Alias111 28% completed...")
					.font(.caption)
				
				// - Color object is also a view. So this is totally aloud
				Color.white.frame(width: 38, height: 6)
					.cornerRadius(3)
					.frame(width: 130, height: 6, alignment: .leading)
					.background(Color(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)).opacity(0.08))
					.cornerRadius(3)
					.padding()
					.frame(width: 150, height: 24)
					.background(Color.black.opacity(0.1))
					.cornerRadius(12)
				/**********************************/
				
				// MARK: -# MENU_ROWS
				///_CHILD__=>MenuRow()<--VStack
				/**********************************/
				//_________
				/// - Account
				MenuRow(icon: "gear", title: "Account")
				/// - Billing
				MenuRow(icon: "creditcard", title: "Billing")
				//__________
				// MARK: - Logout onTapGesture
				///@`````````````````````````````````````````````
				/// - Sign out
				MenuRow(icon: "person.crop.circle", title: "Sign out")
					.onTapGesture( perform: {
						// - Sets "isLoggedIn" to false, so the user is logged out
						UserDefaults.standard.set(false, forKey: "isLoggedIn")
						user.isLoggedIn = false
						showProfile = false
						isShowingAlert = true
					})
					//__________
					// MARK: - - Alert(): once the user is logged out
					///@`````````````````````````````````````````````
					.alert(isPresented: $isShowingAlert, content: {
						Alert(title: Text("Logged out"),
							  dismissButton: .default(Text("Dismiss")))
					})
					///@-`````````````````````````````````````````````
					//_________
				///@-`````````````````````````````````````````````
				//_________
				/**********************************/
				
			}//////_END__=>VSTACK
			// - Will spread the frame width across the whole screen horizontally
			.frame(maxWidth: 500)
			.frame(height: 300)
			.background(BlurView(style: .systemMaterial))
			.clipShape(RoundedRectangle(cornerRadius: 30, style: .continuous))
			.shadow(color: Color.black.opacity(0.2), radius: 20, x: 0, y: 20)
			.padding(.horizontal, 30)
			/**********************************/
			// - Can overlay any view/object. Think ZStack wink😉
			.overlay(
				Image("babygirl").resizable()
					.aspectRatio(contentMode: .fill)
					.frame(width: 60, height: 60)
					// - Gives the image a circular frame
					.clipShape(Circle())
					// - Moves the image up in the `y` axis ⬆️
					.offset(y: -150)
			)
			/**********************************/
		}//||END__PARENT-VSTACK||
		.padding(.bottom, 30)
		//*****************************/
		
	}///-||End Of body||
	/*©-----------------------------------------©*/
}// END: [STRUCT]
