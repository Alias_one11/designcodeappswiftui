import SwiftUI

// MARK: - COMPONENTS
///*©-----------------------------------------©*/
struct MenuRow: View {
	// MARK: - ©PROPERTIES
	var icon,
		title: String
	
	var body: some View {
		///|__________|
		HStack(spacing: 16) {
			// - SFSYMBOL_Image
			Image(systemName: icon)
				.font(.system(size: 20, weight: .light))
				.imageScale(.large)
				.frame(width: 32, height: 32)
				.foregroundColor(Color(#colorLiteral(red: 0.662745098, green: 0.7333333333, blue: 0.831372549, alpha: 1)))
			
			// - ACCOUNT_Text
			Text(title)
				.font(.system(size: 20, weight: .bold, design: .default))
				.frame(width: 120, alignment: .leading)
		}///_END__=>HSTACK
	}
}// END: [STRUCT]
