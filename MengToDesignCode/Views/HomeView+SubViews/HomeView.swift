import SwiftUI

// MARK: - Preview
struct HomeView_Previews: PreviewProvider {
	
	static var previews: some View {
		/// - |.constant(false)|
		/// - Use this method to create a binding to a
		/// - value that cannot change. This can be useful
		/// - when using a PreviewProvider to see how a
		/// - view represents different values.
		HomeView(showProfile: .constant(false),
				 showContent: .constant(false),
				 viewState: .constant(.zero))
			.environmentObject(UserStore())
			//.padding(.all, 100)
			//.preferredColorScheme(.dark)
			//.previewLayout(.sizeThatFits)
			.previewLayout(.fixed(width: 340, height: 640))
	}
}


struct HomeView: View {
	/// MARK: - ©Global-PROPERTIES
	/*****************************/
	// MARK: - ©Binding_PROPERTIES
	@Binding var showProfile: Bool
	@Binding var showContent: Bool
	@Binding var viewState: CGSize
	
	// MARK: - ©State_PROPERTIES
	@State private var showUpdate: Bool = false
	@State private var active: Bool = false
	@State private var activeIndex: Int = -1
	@State private var activeView: CGSize = CGSize.zero
	@State private var isScrollable: Bool = false
	

	
	// MARK: - ©ObservedObject-->PROPERTIES
	@ObservedObject var store: CourseStore = CourseStore()
	
	// MARK: - ©Environment-->PROPERTIES
	@Environment(\.horizontalSizeClass) var horizontalSizeClass
	/*****************************/
	
	/*©-----------------------------------------©*/
	var body: some View {
		
		//||_PARENT__=>VSTACK||
		//*****************************/
		GeometryReader { bounds in
			///__________
			///_CHILD__=>ScrollView
			/*****************************/
			//_________
			ScrollView {
				//__________
				///_CHILD__=>VStack
				/*****************************/
				//_________
				VStack(spacing: 8) {
					///|__________|
					///_CHILD__=>HStack
					/*****************************/
					//_________
					HStack(spacing: 20.0) {
						///__________
						///_CHILD__=>"Watching"<--HStack
						/**********************************/
						//_________
						Text("Watching")
							.bold()
							// .font(.system(size: 28, weight: .bold))
							///__________
							// - Custom font
							.modifier(
								CustomFontModifier(customFont: "OvertheRainbow", size: 28)
							)
						/**********************************/
						
						Spacer()/// Spaced Horizontally
						
						///_CHILD__=>AvatarView() && Button<--HStack
						/**********************************/
						// MARK: - #AvatarView()
						AvatarView(showProfile: $showProfile)
						
						// MARK: - #Bell_Button
						Button(action: { showUpdate.toggle() },
							   label: {
								Image(systemName: "paintbrush")
									//								.renderingMode(.original)
									.foregroundColor(.primary)
									.font(.system(size: 16, weight: .medium))
									.frame(width: 36, height: 36)
									.background(Color("background3"))
									// - Must clip the image before adding shadow
									.clipShape(Circle())
									// - Custom shadow
									.shadow(color: Color.black.opacity(0.1),
											radius: 1, x: 0, y: 1)
									.shadow(color: Color.black.opacity(0.2),
											radius: 10, x: 0, y: 10)
								///*------------------------------*/
								
							   }
						)
						/// - isPresented: A binding to whether the sheet
						/// - is presented.
						/// - onDismiss?: A closure executed when the
						/// - sheet dismisses.
						///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
						.sheet(isPresented: $showUpdate) {
							///|__________|
							UpdateListView()
						}
						///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
						
						
					}///_END__=>HSTACK
					.padding(.horizontal)
					.padding(.leading, 14)
					.padding(.top, 30)
					.blur(radius: active ? 20 : 0)
					///*------------------------------*/
					//__________
					/**********************************/
					
					///_CHILD__=>ScrollView() && WatchRingsView()<--VStack
					/**********************************/
					// MARK: - @WatchRingsView()
					ScrollView(.horizontal, showsIndicators: false) {
						WatchRingsView()
							.padding(.horizontal, 30)
							// - Un clutters the content coming from the bottom
							.padding(.bottom, 30)
							///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
							.onTapGesture {
								// - No toggle since re are not returning to the
								// - orginal state of the content we want to show
								showContent = true
							}
						///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
					}
					.blur(radius: active ? 20 : 0)
					///*------------------------------*/
					
					
					// MARK: - #SectionView()
					/**©-----------------------------------------------------©*/
					ScrollView(.horizontal, showsIndicators: false) {
						HStack(spacing: 20) {
							ForEach(sectionData) { section in
								///|__________|
								// - A container view that defines its content as
								// - a function of its own size and coordinate space.
								GeometryReader { geometry in
									// MARK: - ©SectionView()#1
									SectionView(section: section)
										/// - Will 3d rotate the cards in
										/// - given axis/direction
										///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
										.rotation3DEffect(
											Angle(degrees:
													Double(
														geometry
															.frame(in: .global)
															.minX - 30) / -getAngleMultiplier(bounds: bounds)
											), axis: (x: 0.0, y: 10.0, z: 0.0)
										)
									///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
								}
								// GeometryReader needs to know the size of the frame
								.frame(width: 275, height: 275)
								///*------------------------------*/
							}//_END__=>ForEach
							///|__________|
						}///_END__=>HSTACK
						.padding(30)
						.padding(.bottom, 30)
						///*------------------------------*/
					}
					.offset(y: -30)
					.blur(radius: active ? 20 : 0)
					
					///_CHILD__=>HStack
					/*****************************/
					// MARK: - ©Courses
					
					HStack {
						Text("Courses")
							.modifier(
								CustomFontModifier(customFont: "OvertheRainbow", size: 30)
							)
						Spacer()/// Spaced horizontally
					}
					.padding(.leading, 30)
					.offset(y: -70)
					// - Adds a blur effect
					.blur(radius: active ? 20 : 0)
					/*****************************/
					
					///_CHILD__=>VStack<--ScrollView
					/**********************************/
					//_________
					VStack(spacing: 30) {
						///_CHILD__=>ForEach
						/*****************************/
						//_________
						ForEach(store.courses.indices, id: \.self) { index in
							///__________
							GeometryReader { geometry in
								// - Now it shouldnt matter which card you tap,
								// - the state changes for both cards
								// - Since your bound to a single state,
								// - at the CourseView() level
								CourseView(course: store.courses[index],
										   index: index, bounds: bounds,
										   isScrollable: $isScrollable,
										   show: $store.courses[index].show,
										   active: $active,
										   activeIndex: $activeIndex,
										   activeView: $activeView)
									// - Fills the gap or else do not change anything
									// - When the bottom card is pressed it will move up
									// - to where the top card is
									.offset(y: store.courses[index].show ? -geometry.frame(in: .global).minY : 0)
									// - If not the active card the opacity & scale effect will
									// - scale down those known active cards
									.opacity(activeIndex != index && active ? 0 : 1)
									.scaleEffect(activeIndex != index && active ? 0.5 : 1)
									// - If not the active card, those cards will dissapear to the right
									.offset(x: activeIndex != index && active ? bounds.size.width : 0)
								
							}///END-GeometryReader
							.frame(height: horizontalSizeClass == .regular ? 80 : 280)
							// - Centers card #2
							.frame(maxWidth: store.courses[index].show ?
									712 : getCardWidth(bounds: bounds))
							// - When a card is tapped, any card, that card will take up
							// - all the space on the screen when that sepcific card is tapped.
							.zIndex(store.courses[index].show ? 1 : 0)
							
						}///END-ForEach
						
						/*****************************/
						
					}///END-VStack
					.padding(.bottom, 300)
					.offset(y: -60)
					/**********************************/
					
					/// - Pushes all the content to the top
					Spacer()
					
				}
				.frame(width: bounds.size.width)
				.offset(y: showProfile ? -450 : 0)
				
				// MARK: - Animation & effects
				///@`````````````````````````````````````````````
				// Adding 3d effect with the background view & the MenuView card
				.rotation3DEffect(
					Angle(
						degrees: showProfile ?
							Double(viewState.height / 10) - 10 : 0
					), axis: (x: 10.0, y: 0.0, z: 0.0))
				.scaleEffect(showProfile ? 0.9 : 1)
				.animation(
					.spring(response: 0.5, dampingFraction: 0.6,
							blendDuration: 0)
				)
				///@-`````````````````````````````````````````````
				//_________
				///*------------------------------*/
					
			}///_END__=>ScrollView
			.disabled(active && !isScrollable ? true : false)
			///*------------------------------*/
			///__________
		}//||END__PARENT-VSTACK||
		//*****************************/
	}///-||End Of body||
	/*©-----------------------------------------©*/
}// END: [STRUCT]

func getAngleMultiplier(bounds: GeometryProxy) -> Double {
	///__________
	let boundsWidth: CGFloat = 500
	let boundsToReturn: Double = 80
	let boundsWidthDefault: Double = 20
	
	return bounds.size.width > boundsWidth ?
		boundsToReturn : boundsWidthDefault
}
