import SwiftUI

struct SectionView: View {
	// MARK: - ©Global-PROPERTIES
	/*****************************/
	var section: Section
	var width: CGFloat = 275
	var height: CGFloat = 275
	/*****************************/
	
	var body: some View {
		VStack {
			///|__________|
			HStack(alignment: .top) {
				// - TITLE
				Text(section.title)
					.font(.system(size: 24, weight: .bold))
					.frame(width: 160, alignment: .leading)
					.foregroundColor(.white)
				// - LOGO
				Spacer()/// Spaced horizontally
				Image(section.logo)
				
			}///_END__=>HSTACK
			///*------------------------------*/
			
			// - Text sub heading
			Text(section.text.uppercased())
				.frame(maxWidth: .infinity, alignment: .leading)
			// - Card Image
			section.image
				.resizable()
				.aspectRatio(contentMode: .fit)
				.frame(width: 210)
			
		}///_END__=>VSTACK
		.padding(.top, 20).padding(.horizontal, 20)
		.frame(width: width, height: height)
		.background(section.color)
		.cornerRadius(30)
		// - Custom shadow
		.shadow(color: section.color.opacity(0.3), radius: 20, x: 0, y: 20)
	}
}
