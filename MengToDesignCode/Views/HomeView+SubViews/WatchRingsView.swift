import SwiftUI

struct WatchRingsView_Previews: PreviewProvider {
	
	static var previews: some View {
		
		WatchRingsView()//.padding(.all, 100)
			//.preferredColorScheme(.dark)
		//.previewLayout(.sizeThatFits)
		//.previewLayout(.fixed(width: 540, height: 640))
	}
}

struct WatchRingsView: View {
	// MARK: - ©Global-PROPERTIES
	/*****************************/
	
	/*****************************/

	/*©-----------------------------------------©*/
	var body: some View {
		// MARK: - ©Local-PROPERTIES
		/*****************************/
		
		/*****************************/
		
		HStack(spacing: 30.0) {
			// MARK: - ©HStack#1
			///__________
			HStack(spacing: 12.0) {
				RingView(color1: #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1), color2: #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1), width: 44,
						 height: 44, percent: 68,
						 show: .constant(true))
				
				///_CHILD__=>VStack#1
				/*****************************/
				VStack(alignment: .leading, spacing: 4.0) {
					Text("6 minutes left")
						.bold()
						.modifier(FontModifier(style: .subheadline))
					
					Text("Watched 10 minutes today")
						.modifier(FontModifier(style: .caption))
				}
				/*****************************/
			}
			.padding(8)
			.background(Color("background3"))
			.cornerRadius(20)
			// - Double shadowX2
			.modifier(ShadowModifier())
			
			///_CHILD__=>VStack#2
			/*****************************/
			HStack(spacing: 12.0) {
				RingView(color1: #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1), color2: #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1), width: 32,
						 height: 32, percent: 56,
						 show: .constant(true))
				
			}
			.padding(8)
			.background(Color("background3"))
			.cornerRadius(20)
			// - Double shadowX2
			.modifier(ShadowModifier())
			/*****************************/
			
			///_CHILD__=>VStack#3
			/*****************************/
			HStack(spacing: 12.0) {
				RingView(color1: #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1), color2: #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1), width: 32,
						 height: 32, percent: 32,
						 show: .constant(true))
				
			}
			.padding(8)
			.background(Color("background3"))
			.cornerRadius(20)
			// - Double shadowX2
			.modifier(ShadowModifier())
			/*****************************/
			
		}
	}
	/*©-----------------------------------------©*/
}// END: [STRUCT]
