import SwiftUI

// MARK: - COMPONENTS
///*©-----------------------------------------©*/
struct RingView: View {
	// MARK: - ©Global-PROPERTIES
	/*****************************/
	var color1: UIColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
	var color2: UIColor = #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1)
	var width: CGFloat = 300
	var height: CGFloat = 300
	var percent: CGFloat = 88
	
	// MARK: - ©Binding-->PROPERTIES
	/// - The property has to be of type @Binding
	/// - to be able to work outside of the object
	@Binding var show: Bool
	
	/*****************************/

	/**©---------------------------------------------------------------©*/
	// MARK: - Helper methods
	
	
	/*©-----------------------------------------©*/
	var body: some View {
		// MARK: - ©Local-PROPERTIES
		/*****************************/
		let multiplier = width / 44
		// - Help to render the percentage correctly, with a CGFloat
		let progress = 1 - (percent / 100)
		/*****************************/

		return ZStack {
			
			// - Outer Circle
			Circle()
				.stroke(
					Color.black.opacity(0.1),
					style: StrokeStyle(lineWidth: 5 * multiplier)
				)
				.frame(width: width, height: height)
			
			// - Inner Circle
			/*****************************/
			Circle()
				// - Handles the progress bar loading render
				.trim(from: show ? progress : 1, to: 1)
				.stroke(
					LinearGradient(
						gradient: Gradient(colors: [ Color(color1), Color(color2) ]),
						startPoint: .topTrailing, endPoint: .bottomLeading
					),
					style:
						StrokeStyle(
							lineWidth: 5 * multiplier,
							lineCap: .round,
							lineJoin: .round,
							miterLimit: .infinity,
							dash: [ 20, 0 ],
							dashPhase: 0
						))///_END__=.stroke
				///__________
				// - Adding a rotation effect & 3d effect
				.rotationEffect(Angle(degrees: 90))
				.rotation3DEffect(Angle(degrees: 180), axis: (x: 1.0, y: 0.0, z: 0.0))
				.frame(width: width, height: height)
				// - Custom shadow
				.shadow(color: Color(color2).opacity(0.1), radius: 3 * multiplier, x: 0, y: 3 * multiplier)
				// - Will animate the progess on the bar when tapped
//				.animation(.easeInOut)
				///*------------------------------*/
			
			// - Percentage % loaded. Will appear on the z axis inside both circles
			/*****************************/
			Text("\(Int(percent))%")
				.font(.system(size: 14 * multiplier))
				.fontWeight(.bold)
				// - Will toggle the progress bar ring, off & on, when tapped
				///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
				.onTapGesture {
					show.toggle()
				}
				///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
			///
				///*------------------------------*/
		}//_END__=>ZSTACK
	}
	/*©-----------------------------------------©*/
}// END: [STRUCT]

struct RingView_Previews: PreviewProvider {
	
	static var previews: some View {
		/// - Use this method to create a binding to a value that cannot
		/// - change. This can be useful when using a PreviewProvider to
		/// - see how a view represents different values.
		RingView(show: .constant(true))//.padding(.all, 100)
			//.preferredColorScheme(.dark)
			//.previewLayout(.sizeThatFits)
			.previewLayout(.fixed(width: 540, height: 640))
	}
}
