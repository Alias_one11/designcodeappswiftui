import SwiftUI

struct Post: Codable, Identifiable {
	// MARK: - ©Global-PROPERTIES
	/*****************************/
	let id: UUID = UUID()// Must use coding keys
	/*©----------------------©*/
	var title: String
	var	body: String
	
	/*****************************/
	
	enum CodingKeys: String, CodingKey {
		case id = "id"
		case title = "title"
		case body = "body"
	}
}

/*©-----------------------------------------©*/
class API {
	
	// MARK: - getPosts
	///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	func getPosts(completion: @escaping ([Post]) -> Void) {
		// 1) - URL
		let url = URL(string: "https://jsonplaceholder.typicode.com/posts")
		
		guard let finalURL = url else { return }
		
		// 2) - Data-Task
		URLSession.shared.dataTask(with: finalURL) { (data, _, error) in
			// 3) - Error-Handling
			if let error = error {
				return printf("No data..\n \(error.localizedDescription)")
			}
			
			// 4) - Check for Data
			guard let data = data else { return }
			
			// 5) - Decode-Data
			let d = JSONDecoder()
			
			do {
				let post = try d.decode([Post].self, from: data)
				DispatchQueue.main.async {
					printf(post)
					completion(post)
				}
			} catch {
				printf("Error. No data found\n..\(error)")
			}
		///__________
		}.resume()
	}
		///@-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	//_________
}
